<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\ArtisanDispatchable\Jobs\ArtisanDispatchable;

use Junges\Kafka\Facades\Kafka;

class LaravelKafkaListener implements ShouldQueue, ArtisanDispatchable
{
    
    public function handle()
    {
        echo("Halo Kafka\n");

        $consumer = Kafka::createConsumer()->subscribe('inquiry-request')
        ->withHandler(function(\Junges\Kafka\Contracts\KafkaConsumerMessage $message) {
            var_dump($message);
            $body = $message->getBody();
            echo("Message : ".$body['waktu']."\n");
            
        })->build()->consume();
    }
}
