<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InquiryPlnController;
use App\Http\Controllers\InquiryBpjsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/inquiry/pln/{id}', [InquiryPlnController::class, 'inquiry']);
Route::get('/inquiry/bpjs/{id}', [InquiryBpjsController::class, 'inquiry']);