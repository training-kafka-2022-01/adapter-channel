<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;

class InquiryBpjsController extends Controller
{
    function inquiry(){

        $data = array(
            'client_id' => 'cl001',
            'product_id' => 'bpjs-kes',
            'customer_id' => '0987654321',
            'amount' => 35000,
            'tax' => 3500,
            'penalty' => 0,
            'response_code' => '00'
        );

        // kirim json ke kafka
        $message = new Message(
            body: $data
        );
        Kafka::publishOn('inquiry-request')
            ->withMessage($message)
            ->send();

        return response()->json($data);
    }
}
