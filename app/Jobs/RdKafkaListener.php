<?php

namespace App\Jobs;

use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\ArtisanDispatchable\Jobs\ArtisanDispatchable;
use RdKafka;

class RdKafkaListener implements ShouldQueue, ArtisanDispatchable
{
    
    public function handle()
    {
        echo("Halo Kafka\n");

        $conf = new RdKafka\Conf();
        $conf->set('group.id', 'adapter-channel');
        $conf->set('metadata.broker.list', 'localhost:9092');
        $conf->set('auto.offset.reset', 'earliest');

        $consumer = new RdKafka\KafkaConsumer($conf);
        $consumer->subscribe(['inquiry-request']);
        while(true) {
            $message = $consumer->consume(120*1000);

            switch ($message->err) {
                case RD_KAFKA_RESP_ERR_NO_ERROR:
                    var_dump($message);
                    $consumer->commit();
                    echo("\nMessage : $message->payload \n\n");
                    break;
                case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                    echo "No more messages; will wait for more\n";
                    break;
                case RD_KAFKA_RESP_ERR__TIMED_OUT:
                    echo "Timed out\n";
                    break;
                default:
                    throw new \Exception($message->errstr(), $message->err);
                    break;
            }
        }
    }
}
